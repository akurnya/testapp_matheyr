require 'rubygems'
require 'oauth'
require 'json'

id = '266270116780576768'

tweet = nil

# Now you will fetch /1.1/statuses/show.json, which
# takes an 'id' parameter and returns the
# representation of a single Tweet.
baseurl = "https://api.twitter.com"
path    = "/1.1/statuses/show.json"
query   = URI.encode_www_form('id' => id)
address = URI("#{baseurl}#{path}?#{query}")
request = Net::HTTP::Get.new address.request_uri

# Print data about a Tweet
def print_tweet(tweet)
  # ADD CODE TO PRINT THE TWEET IN "<screen name> - <text>" FORMAT
  username = tweet['user']['name']
  txt = tweet['text']
  puts "#{username} - #{txt}"
end

# Set up HTTP.
http             = Net::HTTP.new address.host, address.port
http.use_ssl     = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

# If you entered your credentials in the first
# exercise, no need to enter them again here. The
# ||= operator will only assign these values if
# they are not already set.
consumer_key ||= OAuth::Consumer.new "", ''
access_token ||= OAuth::Token.new "", ''

#puts consumer_key
#puts access_token

# Issue the request.
request.oauth! http, consumer_key, access_token
http.start
response = http.request request

# Parse and print the Tweet if the response code was 200
if response.code == '200' then
  tweet = JSON.parse(response.body)
  #puts JSON.pretty_generate(tweet)
  print print_tweet(tweet)
end